Réseau IP STATIQUE :

sudo cd /etc/sysconfig/network-scripts/
sudo vi ifcfg-enp0s8

TYPE=ethernet
NAME=enp0s8          
DEVICE=enp0s8        
BOOTPROTO=static     
ONBOOT=yes           
IPADDR=192.168.56.2
NETMASK=255.255.255.0

sudo nmcli con reload
sudo nmcli con up enp0s8

ip a 
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:3b:3e:e0 brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.2/24 brd 192.168.56.255 scope global noprefixroute enp0s8


DNS : 

TYPE=ethernet
NAME=enp0s8          
DEVICE=enp0s8        
BOOTPROTO=static     
ONBOOT=yes           
IPADDR=192.168.56.2
NETMASK=255.255.255.0
DNS1=1.1.1.1

sudo nmcli con reload
sudo nmcli con up enp0s8

[rockycesi@node1 ~]$ ping google.com
PING google.com (172.217.18.206) 56(84) bytes of data.
64 bytes from ham02s14-in-f206.1e100.net (172.217.18.206): icmp_seq=1 ttl=109 time=67.3 ms
64 bytes from ham02s14-in-f206.1e100.net (172.217.18.206): icmp_seq=2 ttl=109 time=65.1 ms
^C
--- google.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 65.125/66.236/67.348/1.140 ms

HOSTNAME :

sudo hostname node1.tp1.cesi
hostname 
node1.tp1.cesi

USERS :

sudo cat /etc/sudoers
utiliser visudo ( QUE POUR MODIF CE FICHIER ) pour modifier le fichier /etc/sudoers

Créer USER :

sudo useradd rockycesi -g wheel
sudo passwd rockycesi => definir le mdp

[rockycesi@node1 home]$ ls
alix  alix2  rockycesi

pour vérifier que l'utilisateur créé est dans le groupe wheel
[rockycesi@node1 home]$ groups
wheel


SELinux : 

sudo vi /etc/seliux/config
# This file controls the state of SELinux on the system.
# SELINUX= can take one of these three values:
#     enforcing - SELinux security policy is enforced.
#     permissive - SELinux prints warnings instead of enforcing.
#     disabled - No SELinux policy is loaded.
SELINUX=permissive
# SELINUXTYPE= can take one of these three values:
#     targeted - Targeted processes are protected,
#     minimum - Modification of targeted policy. Only selected processes are protected.
#     mls - Multi Level Security protection.
SELINUXTYPE=targeted


[rockycesi@node1 home]$ sestatus
SELinux status:                 enabled
SELinuxfs mount:                /sys/fs/selinux
SELinux root directory:         /etc/selinux
Loaded policy name:             targeted
Current mode:                   permissive
Mode from config file:          permissive
Policy MLS status:              enabled
Policy deny_unknown status:     allowed
Memory protection checking:     actual (secure)
Max kernel policy version:      33





Analyse du service SSH :

sudo systemctl status sshd
● sshd.service - OpenSSH server daemon
   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
   Active: active (running) since Mon 2021-12-06 11:31:23 CET; 1h 29min ago

sudo ss -lutpn

[rockycesi@node1 ~]$ sudo vi /etc/ssh/sshd_config
#       $OpenBSD: sshd_config,v 1.103 2018/04/09 20:41:22 tj Exp $

# This is the sshd server system-wide configuration file.  See
# sshd_config(5) for more information.

# This sshd was compiled with PATH=/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin

# The strategy used for options in the default sshd_config shipped with
# OpenSSH is to specify options with their default value where
# possible, but leave them commented.  Uncommented options override the
# default value.

# If you want to change the port on a SELinux system, you have to tell
# SELinux about this change.
# semanage port -a -t ssh_port_t -p tcp #PORTNUMBER
#
Port 4444
#AddressFamily any
#ListenAddress 0.0.0.0
#ListenAddress ::

Ci dessus la modification du port 22 en 4444

Ne pas oublier d'ouvrir le port firewall et de reload



Après modif :
tcp      LISTEN     0          128                  0.0.0.0:4444              0.0.0.0:*      
  users:(("sshd",pid=2336,fd=4))

Pour Vérifier l'utilisateur qui a lancé le processus SSH :
ps -ef 
UID          PID    PPID  C STIME TTY          TIME CMD
rockyce+    2236    2215  0 11:28 ?        00:00:02 sshd: rockycesi@pts/0

J'ouvre maintenant Powershell pour accèder à la machine en SSH : 
PS> ssh rockycesi@192.168.56.2 -p 4444



Service Web :

sudo dnf install nginx
sudo systemcl start nginx
sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-12-06 11:32:29 CET; 1h 23min ago
Vérification port : 
ss -lutpn 
  t= TCP socket
  u= UDP socket
  p= show process using socket
  l= diplay only listening socket
  n= show exact bandwith values
tcp      LISTEN     0          128                  0.0.0.0:80                0.0.0.0:*   
 users:(("nginx",pid=10239,fd=8),("nginx",pid=10238,fd=8))

sudo firewall-cmd --permanent --add-port=80/tcp
 
 aller sur http://192.168.56.2 pour accéder a NGINX
Welcome to nginx on Rocky Linux!
This is the default index.html page that is distributed with nginx on Rocky Linux. It is located in /usr/share/nginx/html.
  
Python :

installation de python d'abord : sudo dnf install python39

[rockycesi@node1 ~]$ sudo python3 -m http.server 8888
[sudo] password for rockycesi:
Serving HTTP on 0.0.0.0 port 8888 (http://0.0.0.0:8888/) ...
192.168.56.1 - - [06/Dec/2021 12:51:08] "GET / HTTP/1.1" 200 -
192.168.56.1 - - [06/Dec/2021 12:51:08] code 404, message File not found
192.168.56.1 - - [06/Dec/2021 12:51:08] "GET /favicon.ico HTTP/1.1" 404 -
port firewall : sudo firewall-cmd --permanent --add-port=8888/tcp

Création service :







Affiner la configuration du service:

sudo adduser web
sudo passwd web 
On met le mot de passe

sudo mkdir /srv/web
cd /srv/web
sudo nano test.txt ==> on écrit du texte afin de pouvoir récupérer le conteu avec la cmd curL
sudo chown web /srv
sudo ls -al ==> web pour les dossier et ficheirs

sudo nano /etc/systemd/system/web.service

on rajoute 

[Service]
User=web
WorkingDirectory=/srv/web

sudo systemctl daemon-reload

avec le client windows :

curl 10.1.1.20:8888/test.txt

On récupère le contenu du fichier créé
