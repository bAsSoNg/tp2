# Partie 2 : Sécurisation

## Sommaire

- [Partie 2 : Sécurisation](#partie-2--sécurisation)
  - [Sommaire](#sommaire)
- [I. Serveur SSH](#i-serveur-ssh)
  - [1. Conf SSH](#1-conf-ssh)
  - [2. Bonus : Fail2Ban](#2-bonus--fail2ban)
- [II. Serveur Web](#ii-serveur-web)
  - [1. Reverse Proxy](#1-reverse-proxy)
  - [2. HTTPS](#2-https)

# I. Serveur SSH

## 1. Conf SSH

🌞 **Modifier la conf du serveur SSH**

```bash
Sur machine physique : ssh-keygen -t ed25519

Ensuite copier la clé vers le serveur Apache depuis le client :

cat .ssh/id_rsa.pub | ssh rockycesi@192.168.56.4 'cat >> .ssh/authorized_keys'

CHanger les permissions de .ssh :

chmod 640 -R .ssh/

Désactivation de root : 

[rockycesi@web ~]$sudo nano /etc/ssh/sshd_config

PermitRootLogin no
PasswordAuthentification no
```

# II. Serveur Web


## 1. Reverse Proxy


🖥️ **Créez une nouvelle machine : `proxy.tp2.cesi`.** 🖥️

🌞 **Installer NGINX**

```bash
[rockycesi@proxy ~]$sudo dnf install nginx
Complete!
```
🌞 **Configurer NGINX comme reverse proxy**

```bash
[rockycesi@poxy ~]$sudo nano /etc/nginx/conf.d/proxy.conf

server {
    listen 80;
    server_name proxy.tp2.cesi;

    location /{
       proxy_pass http://web.tp2.cesi/nextcloud/index.php;
    }
}
```

🌞 **Une fois en place, text !**
```bash
Editez le fichier vhosts
10.2.1.13 web.tp2.cesi
```
## 2. HTTPS

🌞 **Générer une clé et un certificat avec la commande suivante :**

```bash
[rockycesi@proxy ~]$sudo openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -keyout server.key -out server.crt


server.crt  server.key
```


🌞 **Allez, faut ranger la chambre**

```bash
[rockycesi@proxy ~]$sudo cp server.crt /etc/pki/tls/certs/web.tp2.cesi.crt
[rockycesi@proxy ~]$sudo cp server.key /etc/pki/tls/private/web.tp2.cesi.key
```

🌞 **Affiner la conf de NGINX**

```bash
[rockycesi@proxy ~]$sudo nano /etc/nginx/conf.d/proxy.conf
# cette ligne 'listen', vous l'avez déjà. Remplacez-la.
listen                  443 ssl http2;

# nouvelles lignes
# remplacez les chemins par la clé et le cert que vous venez de générer
ssl_certificate         /etc/pki/tls/certs/web.tp2.cesi.crt;
ssl_certificate_key     /etc/pki/tls/private/web.tp2.cesi.key;



[rockycesi@proxy ~]$sudo firewall-cmd --add-port=443/tcp --permanent
success! 
[rockycesi@proxy ~]$sudo firewall-cmd --reload
success!             
```                       

