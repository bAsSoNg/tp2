# Partie 1 : Mise en place de la solution

**Z'est bardi z'est bardiii.**

## Sommaire

- [Partie 1 : Mise en place de la solution](#partie-1--mise-en-place-de-la-solution)
  - [Sommaire](#sommaire)
- [I. Setup base de données](#i-setup-base-de-données)
  - [1. Install MariaDB](#1-install-mariadb)
  - [2. Conf MariaDB](#2-conf-mariadb)
  - [3. Test](#3-test)
- [II. Setup Apache](#ii-setup-apache)
  - [1. Install Apache](#1-install-apache)
    - [A. Apache](#a-apache)
    - [B. PHP](#b-php)
  - [2. Conf Apache](#2-conf-apache)
- [III. NextCloud](#iii-nextcloud)
  - [4. Test](#4-test)

# I. Setup base de données

## 1. Install MariaDB

🌞 **Installer MariaDB sur la machine `db.tp2.cesi`**

```bash
[rockycesi@node1 ~]$ sudo dnf install mariadb-server
[sudo] password for rockycesi:
Last metadata expiration check: 23:06:52 ago on Mon 06 Dec 2021 02:55:33 PM CET.
Package mariadb-server-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!
```

🌞 **Le service MariaDB**

```bash
[rockycesi@node1 ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.

[rockycesi@node1 ~]$ sudo systemctl start mariadb.service
[rockycesi@node1 ~]$ sudo systemctl status mariadb.service
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-07 14:05:01 CET; 2s ago

sudo ss -lutpn  | grep "mysql"
tcp   LISTEN 0      80                 *:3306            *:*    users:(("mysqld",pid=2769,fd=21))

[rockycesi@node1 ~]$ sudo ps -q 2769
    PID TTY          TIME CMD
   2769 ?        00:00:00 mysqld

[rockycesi@node1 ~]$ sudo ps -ef
UID          PID    PPID  C STIME TTY          TIME CMD
mysql       2769       1  0 14:05 ?        00:00:00 /usr/libexec/mysqld --basedir=/usr


```

🌞 **Firewall**

```bash
[rockycesi@node1 ~]$ sudo firewall-cmd --permanent --add-port=3306/tcp
success
[rockycesi@node1 ~]$ sudo firewall-cmd --reload
success
```

## 2. Conf MariaDB

Première étape : le `mysql_secure_installation`. C'est un binaire (= une commande, une application, un programme, ces mots désignent la même chose) qui sert à effectuer des configurations très récurrentes, on fait ça sur toutes les bases de données à l'install.  
C'est une question de sécu.

🌞 **Configuration élémentaire de la base**


```bash
[rockycesi@node1 ~]$ sudo mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.


Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

You already have a root password set, so you can safely answer 'n'.

Change the root password? [Y/n] n
 ... skipping.

By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```

🌞 **Préparation de la base en vue de l'utilisation par NextCloud**


```bash
[rockycesi@node1 ~]$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 21
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]>
```


```bash

MariaDB [(none)]> CREATE USER 'nextcloud'@'192.168.56.4' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'192.168.56.4';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)

```

## 3. Test

➜ **On va tester que la base sera utilisable par NextCloud.**

Concrètement il va faire quoi NextCloud vis-à-vis de la base MariaDB ?

- se connecter sur le port où écoute MariaDB
- la connexion viendra de `web.tp2.cesi`
- il se connectera en utilisant l'utilisateur `nextcloud`
- il écrira/lira des données dans la base `nextcloud`

➜ Il faudrait donc qu'on teste ça, à la main, **depuis la machine `web.tp2.cesi`**.

Bah c'est parti ! Il nous faut juste un client pour nous connecter à la base depuis la ligne du commande : il existe une commande `mysql` pour ça.

🌞 **Installez sur la machine `web.tp2.cesi` la commande `mysql`**

```bash
[rockycesi@web ~]$ sudo dnf provides mysql
[sudo] password for rockycesi:
Last metadata expiration check: 1 day, 0:08:11 ago on Mon 06 Dec 2021 02:55:33 PM CET.
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7

[rockycesi@web ~]$sudo dnf install mysql
Installed:
  mariadb-connector-c-config-3.1.11-2.el8_3.noarch               mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64
  mysql-common-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64

Complete!
[rockycesi@web ~]$
```


🌞 **Tester la connexion**


```bash
[rockycesi@web ~]$ sudo mysql -u nextcloud -p  -h 192.168.56.3 nextcloud
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 43
Server version: 5.5.5-10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> SHOW TABLES;
Empty set (0.00 sec)
```

C'est bon ! Ca tourne ! **On part sur Apache maintenant !**

# II. Setup Apache

> La section II et III sont clairement inspirés de [la doc officielle de Rocky pour installer NextCloud](https://docs.rockylinux.org/guides/cms/cloud_server_using_nextcloud/).

Comme annoncé dans l'intro, on va se servir d'Apache dans le rôle de serveur Web dans ce TP5. Histoire de varier les plaisirs è_é

![Linux is a tipi](./pics/linux_is_a_tipi.jpg)

## 1. Install Apache

### A. Apache

🌞 **Installer Apache sur la machine `web.tp2.cesi`**

```bash
[rockycesi@web ~]$ sudo dnf install httpd
Installed:
  apr-1.6.3-12.el8.x86_64
  apr-util-1.6.1-6.el8.1.x86_64
  apr-util-bdb-1.6.1-6.el8.1.x86_64
  apr-util-openssl-1.6.1-6.el8.1.x86_64
  httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64
  httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch
  httpd-tools-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64
  mod_http2-1.15.7-3.module+el8.5.0+695+1fa8055e.x86_64
  rocky-logos-httpd-85.0-3.el8.noarch

Complete!
```

🌞 **Analyse du service Apache**


```bash
[rockycesi@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.

[rockycesi@web ~]$ sudo systemctl start httpd

[rockycesi@web ~]$ sudo ss -lutpn | grep "httpd"
tcp   LISTEN 0      128                *:80              *:*    users:(("httpd",pid=2395,fd=4),("httpd",pid=2394,fd=4),("httpd",pid=2393,fd=4),("httpd",pid=2387,fd=4))

[rockycesi@web ~]$ sudo ps -ef
apache      2392    2387  0 15:42 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2393    2387  0 15:42 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2394    2387  0 15:42 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2395    2387  0 15:42 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
```
- le service aussi s'appelle `httpd`
- lancez le service `httpd` et activez le au démarrage
- isolez les processus liés au service `httpd`
- déterminez sur quel port écoute Apache par défaut
- déterminez sous quel utilisateur sont lancés les processus Apache

---

🌞 **Un premier test**

```bash
[rockycesi@web ~]$ sudo firewall-cmd --permanent --add-port=443/tcp
success
[rockycesi@web ~]$ sudo firewall-cmd --reload
success

PS C:\Users\a.lemoine> curl 192.168.56.4
curl : HTTP Server Test Page
This page is used to test the proper operation of an HTTP server after it has been installed on a Rocky Linux system.
If you can read this page, it means that the software it working correctly.
Just visiting?
This website you are visiting is either experiencing problems or could be going through maintenance.
If you would like the let the administrators of this website know that you've seen this page instead of the page
you've expected, you should send them an email. In general, mail sent to the name "webmaster" and directed to the
website's domain should reach the appropriate person.
The most common email address to send to is: "webmaster@example.com"
Note:
[.....]
```

- ouvrez le port d'Apache dans le firewall
- testez, depuis votre PC, que vous pouvez accéder à la page d'accueil par défaut d'Apache
  - avec une commande `curl`
  - avec votre navigateur Web

### B. PHP

NextCloud a besoin d'une version bien spécifique de PHP.  
Suivez **scrupuleusement** les instructions qui suivent pour l'installer.

🌞 **Installer PHP**

```bash
[rockycesi@web ~]$ sudo dnf install epel-release
Complete!
[rockycesi@web ~]$sudo dnf update
Complete!
[rockycesi@web ~]$sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
Complete!
[rockycesi@web ~]$sudo dnf module enable php:remi-7.4
Complete!
[rockycesi@web ~]$sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php- [........]
Complete!
```

## 2. Conf Apache


🌞 **Analyser la conf Apache**

```bash
[rockycesi@web ~]$sudo nano /etc/httpd/conf/httpd.conf ==> IncludeOptional conf.d/*.conf
```

🌞 **Créer un VirtualHost qui accueillera NextCloud**


```bash
[rockycesi@web ~]$sudo touch /etc/httpd/conf.d/vhosts.conf

[rockycesi@web ~]$ls /etc/httpd/conf.d/vhosts.conf ==> /etc/httpd/conf.d/vhosts.conf

<VirtualHost *:80>
  # on précise ici le dossier qui contiendra le site : la racine Web
  DocumentRoot /var/www/nextcloud/html/  

  # ici le nom qui sera utilisé pour accéder à l'application
  ServerName  web.tp2.cesi  

  <Directory /var/www/nextcloud/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```

> N'oubliez pas de redémarrer le service à chaque changement de la configuration, pour que les changements prennent effet.

🌞 **Configurer la racine web**

```bash
[rockycesi@web ~]$sudo mkdir /var/www/nextcloud/
[rockycesi@web ~]$sudo mkdir /var/www/nextcloud/html/
[rockycesi@web ~]$sudo chown apache /var/www/nextcloud/html/

[rockycesi@web ~]$sudo ls -la /var/www/nextcloud/html
drwxr-xr-x. 3 apache root 8 Dec 09:33 .
drwxr-xr-x. 2 apache root 8 Dec 09:33 html
```

🌞 **Configurer PHP**


```bash
[rockycesi@web ~]$sudo nano /etc/opt/ remi/php74/php.ini

date.timezone = "Europe/Paris"
```

# III. NextCloud

On dit "installer NextCloud" mais en fait c'est juste récupérer les fichiers PHP, HTML, JS, etc... qui constituent NextCloud, et les mettre dans le dossier de la racine web.

🌞 **Récupérer Nextcloud**

```bash
[rockycesi@web ~]$sudo curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
[rockycesi@web ~]$sudo ls 
nextcloud-21.0.1.zip
```

🌞 **Ranger la chambre**

```bash
[rockycesi@web ~]$sudo unzip nextcloud-21.0.1.zip
[rockycesi@web ~]$sudo ls
nextcloud-21.0.1.zip nextcloud

[rockycesi@web ~]$sudo rm -f nextcloud-21.0.1.zip

[rockycesi@web ~]$sudo mv /home/sancur/nextcloud /var/www/nextcloud/html/

[rockycesi@web ~]$sudo ls /var/www/nextcloud/html/
nextcloud

[rockycesi@web ~]$sudo chown apache -R /var/www/nextcloud/html/nextcloud/

[rockycesi@web ~]$sudo ls -la 
[ tous les fichiers appartiennent au user apache....] 

```

## 4. Test


🌞 **Modifiez le fichier `hosts` de votre PC**

```bash
Editez le fichier hosts et rajouter : 
192.168.56.4 web.tp2.cesi
```

🌞 **Tester l'accès à NextCloud et finaliser son install'**

```bash
Se rendre sur la page http://web.tp2.cesi/nextcloud/index.php

Créer l'admin : rockycesi + Mdp

Répertoire BDD : 

/var/www/nextcloud/html/data

Choisir mariadb comme BDD : 

nextcloud : User
Mdp : meow
BDD : nextcloud
IP : 192.168.56.3:3306

Je peux me connecter sur NextCLoud avec le compte créé
```

**🔥🔥🔥 Baboom ! Un beau NextCloud.**

