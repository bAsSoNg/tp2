# Partie 3 : Maintien en condition opérationnelle

## Sommaire

- [Partie 3 : Maintien en condition opérationnelle](#partie-3--maintien-en-condition-opérationnelle)
  - [Sommaire](#sommaire)
- [I. Monitoring](#i-monitoring)
  - [1. Intro](#1-intro)
  - [2. Setup Netdata](#2-setup-netdata)
  - [3. Bonus : alerting](#3-bonus--alerting)
  - [4. Bonus : proxying](#4-bonus--proxying)
- [II. Backup](#ii-backup)

# I. Monitoring

## 2. Setup Netdata


🌞 **Installez Netdata** en exécutant la commande suivante :

```bash
[rockycesi@proxy ~]$sudo bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
```
🌞 **Démarrez Netdata**


```bash
[rockycesi@proxy ~]$sudo systmctl start netdata
[rockycesi@proxy ~]$sudo systmctl enable netdata
[rockycesi@proxy ~]$sudo firewall-cmd --add-port=19999/tcp --permanent
success
[rockycesi@proxy ~]$sudo firewall-cmd --reload
success
```

# II. Backup

🌞 **Téléchargez Borg** sur la machine `web.tp2.cesi`

```bash
[rockycesi@web ~]$sudo curl -SLO https://github.com/borgbackup/borg/releases/download/1.1.17/borg-linux64
[rockycesi@web ~]$sudo cp borg-linux64 /usr/local/bin/borg
[rockycesi@web ~]$sudo chown root:root /usr/local/bin/borg
[rockycesi@web ~]$sudo chmod 755 /usr/local/bin/borg
```

🌞 **Jouer avec Borg**

```bash
[rockycesi@web ~]$sudo borg init --encryption=repokey /srv/backup
[rockycesi@web ~]$sudo borg create /srv/backup::Monday ~/home/rockycesi/Documents

```
🌞 **Ecrire un script**

```bash

sudo nano /home/rockycesi/script.sh

#!/bin/sh

/usr/bin/borg create /srv/backup/nextcloud::nextcloud_211208_163815 ~/var/www/nextcloud/

```

🌞 **Créer un service**

```bash
[rockycesi@web ~]$sudo nano /etx/systemd/system/backup_db.service

[Unit]
Description=<DESCRIPTION>

[Service]
ExecStart=<COMMAND>
Type=oneshot

[Install]
WantedBy=multi-user.target
```

🌞 **Créer un timer**

```bash

[rockycesi@web ~]$sudo nano /etc/systemd/system/backup.timer

[Unit]
Description=Lance backup.service à intervalles réguliers
Requires=backup.service

[Timer]
Unit=backup.service
OnCalendar=hourly

[Install]
WantedBy=timers.target
```


Activez maintenant le *timer* avec :

```bash
[rockycesi@web ~]$sudo systemctl daemon-reload
[rockycesi@web ~]$sudo systemctl start backup.timer
[rockycesi@web ~]$sudo systemctl enable backup.timer
```

🌞 **Vérifier que le *timer* a été pris en compte**, en affichant l'heure de sa prochaine exécution :

```bash
[rockycesi@web ~]$sudo systemctl list-timers
NEXT			    LEFT
Wed 2021-12-08 17:40:45 CET 59min left 
```
               